/**
 * Program to learn git
 * Author: Eduardo Juarez
 * Date: 11 de septiembre 2019
 **/

//Include libraries
#include <stdlib.h> //Standard library
#include <stdio.h> //Standard input / output
#include <assert.h> //Testing library

/**
 * Main function of the program
 */
int main() {
    printf("Este es un programa para el rover\n\n");
    printf("hello world\n");
    printf("Nos faltan 20 min\n");
    printf("esto esta en la rama de lalo\n");
}
